import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";

const CardWrapper = (props) => {
  return (
    <>
      <Card style={{ margin: "0 auto", width: "100%" }} sx={{ maxWidth: 345 }}>
        {props.children}
      </Card>
    </>
  );
};

export default CardWrapper;
