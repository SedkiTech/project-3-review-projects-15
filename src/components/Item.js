import React from "react";
import useStyles from "./ItemStyles";

const Item = (props) => {
  const classes = useStyles();
  return (
    <>
      <h4 className={classes.name}> {props.onSendData.name} </h4>
      <img
        className={classes.image}
        src={props.onSendData.image}
        alt={props.name}
      />
      <p className={classes.job}> {props.onSendData.job} </p>
      <p className={classes.text}> {props.onSendData.text} </p>
    </>
  );
};

export default Item;
