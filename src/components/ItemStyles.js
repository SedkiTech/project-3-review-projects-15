import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  // name: {
  //   color: "black",
  //   padding: "0 30px",
  //   fontWeight: "bold",
  // },
  job: {
    color: "primary",
  },
  // text: {
  //   padding: "6px 12px",
  //   color: "gray",
  // },

  image: {
    borderRadius: "50%",
    width: "30%",
  },
}));

export default useStyles;
