import React from "react";
import Item from "./Item";
const ListItem = (props) => {
  let data = null;
  const sendDatahandler = (data) => {
    return (data = { ...props.data });
  };
  return (
    <div>
      {/* {props.data.map((item) => {
        return <Item key={item.id} {...item} />;
      })} */}

      <Item {...props.data} onSendData={sendDatahandler} />
    </div>
  );
};

export default ListItem;
