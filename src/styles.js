//makeStyles

import { makeStyles } from "@mui/styles";
import { purple } from "@mui/material/colors";

const useStyles = makeStyles((theme) => ({
  buttons: {
    margin: "40px",
  },
  main: {
    margin: "0 auto",
  },

  header: {
    textAlign: "center",
  },
  title: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    borderRadius: 3,
    border: 0,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "black",
    height: 48,
    padding: "0 30px",
  },
  container: {
    backgroundColor: purple[500],
    padding: "6px 12px",
  },
}));

export default useStyles;
