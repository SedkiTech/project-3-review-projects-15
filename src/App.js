import React from "react";

import Review from "./Review";

import "./index.css";

// import Material UI Elements
// import Button from "@mui/material/Button";
// import Components
// import useStyles from "./styles";
// import Review from "./Review";
// import CardWrapper from "./components/UI/CardWrapper";
// import Item from "./components/Item";
// import ListItem from "./components/ListItem";

function App() {
  // const classes = useStyles();
  // const data = Review;

  // const dataLength = data.length;
  // console.log(data);
  // console.log("dataLength", dataLength);

  // const [index, setIndex] = useState(0);

  return (
    <main>
      {/* <h1 className={classes.header}>Our Reviews</h1> */}

      <section className="container">
        <div className="title">
          <h2> Our Reviews</h2>
          <div className="underline"></div>
          <Review />
        </div>
      </section>

      {/* <CardWrapper> */}
      {/* <Item data={data[0]} key={data[0].id} /> */}

      {/* <ListItem data={data} index={index} /> */}

      {/* <Button className={classes.title} variant="contained">
          Random Review
        </Button> */}
      {/* </CardWrapper> */}
    </main>
  );
}

export default App;
